from django.test import TestCase, Client
from django.urls import reverse


from .models import Product

# Create your tests here.


class ProductTests(TestCase):


    def setUp(self):

        self.product = Product.objects.create(

            title = 'python book',
            owner = 'steve',
            description = 'xyza',
            contact = 'xyz',

        )


    def test_product_listing(self):

        self.assertEqual(f'{self.product.title}', 'python book')
        self.assertEqual(f'{self.product.owner}', 'steve')
        self.assertEqual(f'{self.product.description}', 'xyza')
        self.assertEqual(f'{self.product.contact}', 'xyz')

    
    def test_product_list_view(self):

        response = self.client.get(reverse('product_list'))
        
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'python book')
        self.assertContains(response, 'steve')
        self.assertContains(response, 'xyza')
        self.assertContains(response, 'xyz')
        self.assertTemplateUsed(response, 'products/product_list.html')


    def test_product_detail_view(self):

        response = self.client.get(self.product.get_absolute_url())
        no_response = self.client.get('/products/12345/')

        self.assertEqual(response.status_code, 200)
        self.assertEqual(no_response.status_code, 404)
        self.assertContains(response, 'python book')
        self.assertContains(response, 'steve')
        self.assertContains(response, 'xyza')
        self.assertContains(response, 'xyz')
        self.assertTemplateUsed(response, 'products/product_detail.html')
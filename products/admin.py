from django.contrib import admin

from .models import Product, Owner, Contact, Genere

# Register your models here.

class GenereAdmin(admin.ModelAdmin):

    pass# list_display = ('genere_name',)

class OwnerAdmin(admin.ModelAdmin):

    pass #list_display = ('owner_name',)

class ContactAdmin(admin.ModelAdmin):

    pass #list_display = ('phone', 'email')


class ProductAdmin(admin.ModelAdmin):

    pass #list_display = ('title', 'image', 'owner', 'description', 'genere_display')



admin.site.register(Product)
admin.site.register(Owner)
admin.site.register(Contact)
admin.site.register(Genere)
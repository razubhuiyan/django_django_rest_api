from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from .models import Product, Owner, Contact, Genere


class ProductListView(ListView):

    model = Product

    context_object_name = "product_list"

    template_name = "products/product_list.html"


class ProductDetailView(DetailView):

    model = Product, Owner, Contact, Genere

    context_object_name = "product, Owner, Contact, Genere"

    template_name = "products/product_detail.html"
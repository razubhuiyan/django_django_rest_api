from django.db import models
from django.utils import timezone
from django.urls import reverse
import uuid
from phonenumber_field.modelfields import PhoneNumberField 


# Create your models here.

class Owner(models.Model):

    owner_name = models.CharField(max_length=50, help_text="Enter owner name")

    def __str__(self):
        return self.owner_name

class Contact(models.Model):

    phone = PhoneNumberField(null=False, blank=False, unique=True)
    email = models.EmailField(unique=True)
 

class Genere(models.Model):

    genere_name = models.CharField(max_length=50, help_text="Enter the product genere ( e.g. Books, Electronics,  Furniture)")


    def __str__(self):
        return self.genere_name

class Product(models.Model):

    id = models.UUIDField(

        primary_key = True,

        default = uuid.uuid4,
        
        editable = False

    )

    title = models.CharField(max_length=50)
    image = models.ImageField(upload_to='covers/', blank=True)
    owner = models.ForeignKey(Owner, on_delete=models.SET_NULL, null=True)
    description = models.TextField(max_length=200)
    uploadtime= models.DateTimeField(default=timezone.now, editable=False)
    genere = models.ManyToManyField(Genere, help_text="Select a genere for this product")
    objects = models.Manager()

    class Meta:

        ordering = ['title', '-uploadtime']


    def __str__(self):
        return self.title

    def display_genere(self):

        return ', '.join(genere.name for genere in self.genere.all()[:3])

    display_genere.short_description = 'Genere'


    def get_absolute_url(self):
        return reverse("product_detail", kwargs={'pk': str(self.pk)})

    
    
    
    


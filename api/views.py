from django.shortcuts import render
from rest_framework.generics import ListCreateAPIView, RetrieveUpdateDestroyAPIView

from products.models import Product
from .serializers import ProductSerializer

# Create your views here.


class ProductListApiView(ListCreateAPIView):

    queryset = Product.objects.all()

    serializer_class = ProductSerializer


class ProductDetailApiView(RetrieveUpdateDestroyAPIView):

    queryset = Product.objects.all()

    serializer_class = ProductSerializer